# Privacy Policy

This Privacy Policy applies to any of Bertly's apps in the Atlassian Marketplace.

## Usage

### Server

Our apps never store any data outside of your Atlassian system. Therefore, we have no possibility to see any of your data unless you provide us with such.

### Cloud

Our apps are hosted by Atlassian and keep all data in Atlassian's infrastructure. Therefore, we have no possibility to see any of your data unless you or Atlassian provide us with such.

## Support

If you have issues or questions related to our apps, you might want to contact us. When working on your request, we might ask you for additional information to be able to provide appropriate support. When doing so, we only receive the information you explicitly provide to us. We will use this information only to solve your issues and answer your questions.
